﻿namespace GeoFigurBerechnung.Models
{
    // Basisklasse für Flächen- und Umfangsklassen, erbt von Basisklasse CGeomFigur
    public class CFlaeche : CGeomFigur
    {
        // Deklararion der Eigenschaften für Klassen der Flächen- und Umfangsberechnung
        internal double Flaecheninhalt { get; set; }
        internal double Umfang { get; set; }
    }
}
