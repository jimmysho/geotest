﻿namespace GeoFigurBerechnung.Models
{
    // Basisklasse für Volumen- und Oberflächenklassen, erbt von Basisklasse CGeomFigur
    class CKoerper : CGeomFigur
    {
        // Deklararion der Eigenschaften für Klassen der Volumen- und Oberflächenberechnung
        internal double Volumen { get; set; }
        internal double Oberflaeche { get; set; }
    }
}
