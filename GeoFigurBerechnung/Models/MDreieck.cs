﻿using System;
using GeoFigurBerechnung.Interfaces;

namespace GeoFigurBerechnung.Models
{
    // Klasse erbt von CFlaeche
    class MDreieck : CFlaeche, ICFlaeche
    {
        // Deklaration der Eigenschaften
        internal double SeiteA { get; set; }
        internal double SeiteB { get; set; }
        internal double SeiteC { get; set; }

        // Konstruktor mit Übergabe und Aufruf der Flächenberechnung
        public MDreieck(string bezeichnung = "", string farbe = "", double seiteA = 0, double seiteB = 0, double seiteC = 0)
        {
            Bezeichnung = bezeichnung;
            Farbe = farbe;

            SeiteA = seiteA;
            SeiteB = seiteB;
            SeiteC = seiteC;

            Umfangberechnung();
            Flaechenberechnung();
        }

        // Funktion der Flächenberechnung
        public void Flaechenberechnung()
        {
            double i = (SeiteA + SeiteB + SeiteC) / 2.0;
            Flaecheninhalt = Math.Sqrt(i * (i - SeiteA) * (i - SeiteB) * (i - SeiteC));
        }

        // Funktion der Umfangsberechnung
        public void Umfangberechnung()
        {
            Umfang = SeiteA + SeiteB + SeiteC;
        }
    }
}
