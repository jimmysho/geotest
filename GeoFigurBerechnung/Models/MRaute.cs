﻿using GeoFigurBerechnung.Interfaces;
using System;

namespace GeoFigurBerechnung.Models
{
    // Klasse erbt von CFlaeche und von ICFlaeche
    public class MRaute : CFlaeche, ICFlaeche
    {
        // Deklaration der Eigenschaften
        internal double Seitenlaenge { get; set; }
        internal double DiagonaleE { get; set; }
        internal double DiagonaleF { get; set; }

        // Konstruktor mit Kontrollfluss und Aufruf verschiedener Funktionen
        public MRaute(string bezeichnung = "", string farbe = "", double seiteA = 0, double diagonaleE = 0, double diagonaleF = 0)
        {
            Bezeichnung = bezeichnung;
            Farbe = farbe;
            DiagonaleE = diagonaleE == 0 ? DiagonaleBerechnung(seiteA, diagonaleF) : diagonaleE;
            DiagonaleF = diagonaleF == 0 ? DiagonaleBerechnung(seiteA, diagonaleE) : diagonaleF;
            Seitenlaenge = seiteA == 0 ? SeiteABerechnung(diagonaleE, diagonaleF) : seiteA;
            Flaechenberechnung();
            Umfangberechnung();           
        }

        // Funktion Flächenberechnung
        public void Flaechenberechnung()
        {                 
            Flaecheninhalt = (DiagonaleE * DiagonaleF) / 2;           
        }

        // Funktion Umfangsberechnung
        public void Umfangberechnung()
        {
            Umfang = Seitenlaenge * 4;
        }

        // Funktion Seite-A-Berechung
        private double SeiteABerechnung(double e, double f)
        {
            return Math.Sqrt(Math.Pow((e / 2), 2) + Math.Pow((f / 2), 2));           
        }

        // Funktion Diagonaleberechung
        private double DiagonaleBerechnung(double seite, double diagonale)
        {
            return Math.Sqrt((4 * Math.Pow(seite, 2) - Math.Pow(diagonale, 2)));
        }
    }
}
