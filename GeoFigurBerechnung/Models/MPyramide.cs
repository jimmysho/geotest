﻿using GeoFigurBerechnung.Interfaces;
using System;

namespace GeoFigurBerechnung.Models
{
    // Klasse erbt von CKoerper und von ICKoerper
    class MPyramide : CKoerper, ICKoerper
    {
        // Deklaration der Eigenschaften
        internal double Seitenlaenge { get; set; }
        internal double Hoehe { get; set; }
        internal double HoeheA { get; set; }
        internal double Mantelflaeche { get; set; }

        // Konstruktor mit Kontrollfluss und Aufruf von Funktionen zur Berechnung der Parameter
        public MPyramide(string bezeichnung = "", string farbe = "", double seitenlaenge = 0, double hoehe = 0, double volumen = 0, double oberflaeche = 0)
        {
            Bezeichnung = bezeichnung;
            Farbe = farbe;

            if(seitenlaenge > 0 && hoehe > 0)
            {
                Seitenlaenge = seitenlaenge;
                Hoehe = hoehe;
                HoeheABerechnung();
                Mantelflaechenberechnung();
                Volumenberechnung();
                Oberflaechenberechnung();
            }

            else if (seitenlaenge > 0 && volumen > 0)
            {
                Seitenlaenge = seitenlaenge;
                Volumen = volumen;
                Hoehenberechnung();
                HoeheABerechnung();
                Mantelflaechenberechnung();
                Oberflaechenberechnung();
            }

            else if (seitenlaenge > 0 && oberflaeche > 0)
            {
                Seitenlaenge = seitenlaenge;
                Oberflaeche = oberflaeche;
                Mantelflaechenberechnung();
                HoeheABerechnung();
                Hoehenberechnung();
                Volumenberechnung();
            }

            else if (hoehe > 0 && volumen > 0)
            {
                Hoehe = hoehe;
                Volumen = volumen;
                Seitenlaengenberechnung();
                HoeheABerechnung();
                Mantelflaechenberechnung();
                Oberflaechenberechnung();
            }

            else if (hoehe > 0 && oberflaeche > 0)
            {
                Hoehe = hoehe;
                Oberflaeche = oberflaeche;
            }

            else if (volumen > 0 && oberflaeche > 0)
            {
                Volumen = volumen;
                Oberflaeche = oberflaeche;
            }
        }

        // Funktion zum berechnen der Seitenlänge
        public void Seitenlaengenberechnung()
        {
            if (Hoehe > 0 && Volumen > 0) Seitenlaenge = Math.Sqrt(3 * Volumen / Hoehe);
        }

        // Funktion zum berechnen der Höhe
        public void Hoehenberechnung()
        {
            if (Seitenlaenge > 0 && Volumen > 0) Hoehe = (3 * Volumen) / Math.Pow(Seitenlaenge, 2);
            else if (Seitenlaenge > 0 && HoeheA > 0) Hoehe = Math.Sqrt(-0.25 * Math.Pow(Seitenlaenge, 2) + Math.Pow(HoeheA, 2));
        }

        // Funktion zum berechnen des Volumens
        public void Volumenberechnung()
        {
            Volumen = (1d / 3d) * Math.Pow(Seitenlaenge, 2) * Hoehe;
        }

        // Funktion zum berechnen der Oberfläche
        public void Oberflaechenberechnung()
        {
            Oberflaeche = Math.Pow(Seitenlaenge, 2) + Mantelflaeche;
        }

        // Funktion zum berechnen der Höhe der Seite
        public void HoeheABerechnung()
        {
            if (Seitenlaenge > 0 && Hoehe > 0) HoeheA = Math.Sqrt(Math.Pow(Hoehe, 2) + (Math.Pow(Seitenlaenge, 2) / 4));
            else if (Seitenlaenge > 0 && Mantelflaeche > 0) HoeheA = (Mantelflaeche / 2) / Seitenlaenge;
        }

        // Funktion zum berechnen der Mantelfläche
        public void Mantelflaechenberechnung()
        {
            if (Seitenlaenge > 0 && HoeheA > 0) Mantelflaeche = 2 * Seitenlaenge * HoeheA;
            else if (Seitenlaenge > 0 && Oberflaeche > 0) Mantelflaeche = (-1 * Math.Pow(Seitenlaenge, 2)) + Oberflaeche;
        }
    }
}
