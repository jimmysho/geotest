﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeoFigurBerechnung.Interfaces;

namespace GeoFigurBerechnung.Models
{
    // Klasse erbt von CKoerper und von ICKoerper
    class MKugel : CKoerper, ICKoerper
    {
        // Deklaration der Eigenschaft
        internal double Radius { get; set; }

        // Konstruktor mit Kontrollfluss und Aufruf von Funktionen zur Berechnung der Parameter
        public MKugel(string bezeichnung = "", string farbe = "", double radius = 0, double oberflaeche = 0, double volumen = 0)
        {
            Bezeichnung = bezeichnung;
            Farbe = farbe;

            if (radius > 0)
            {
                Radius = radius;
                Volumenberechnung();
                Oberflaechenberechnung();
            }
            else if (oberflaeche > 0)
            {
                Oberflaeche = oberflaeche;
                Radiusberechnung();
                Volumenberechnung();
            }
            else if (volumen > 0)
            {
                Volumen = volumen;
                Radiusberechnung();
                Oberflaechenberechnung();
            }
        }

        // Funktion zum berechnen des Radius
        public void Radiusberechnung()
        {
            if (Volumen > 0)
            {
                Radius = Math.Pow((3 * Volumen) / (4 * Math.PI), (1d / 3d));
            }

            else if (Oberflaeche > 0)
            {
                Radius = Math.Sqrt(Oberflaeche / (4 * Math.PI));
            }
        }

        // Funktion zum berechnen des Volumens
        public void Volumenberechnung()
        {
            Volumen = 4 * Math.PI * Math.Pow(Radius, 3) / 3;
        }

        // Funktion zum brechnen der Oberfläche
        public void Oberflaechenberechnung()
        {
            Oberflaeche = 4 * Math.PI * Math.Pow(Radius, 2);
        }
    }
}
