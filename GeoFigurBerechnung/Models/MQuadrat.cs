﻿using System;
using GeoFigurBerechnung.Interfaces;

namespace GeoFigurBerechnung.Models
{
    // Klasse erbt von CFlaeche
    class MQuadrat : CFlaeche, ICFlaeche
    {
        // Eigenschaft
        internal double Seitenlaenge { get; set; }

        // Konstruktor mit Kontrollfluss und Berechnungen
        public MQuadrat(string bezeichnung = "", string farbe = "", double seitenlaenge = 0, double flaecheninhalt = 0, double umfang = 0)
        {
            Bezeichnung = bezeichnung;
            Farbe = farbe;

            if (flaecheninhalt > 0)
            {
                Flaecheninhalt = flaecheninhalt;
                Seitenlaengeberechnung();
                Umfangberechnung();
            }
            else if (seitenlaenge > 0)
            {
                Seitenlaenge = seitenlaenge;
                Flaechenberechnung();
                Umfangberechnung();
            }
            else if (umfang > 0)
            {
                Umfang = umfang;
                Seitenlaengeberechnung();
                Flaechenberechnung();
            }
        }

        // Funktion zum berechnen des Flächeninhalts
        public void Flaechenberechnung()
        {
            Flaecheninhalt = Seitenlaenge * Seitenlaenge;
        }

        // Funktion zum berechnen des Umfangs
        public void Umfangberechnung()
        {
            Umfang = Seitenlaenge * 4;
        }

        // Funktion zum berechnen der Seitenlänge
        public void Seitenlaengeberechnung()
        {
            if(Flaecheninhalt > 0) Seitenlaenge = Math.Sqrt(Flaecheninhalt);
            else if (Umfang > 0) Seitenlaenge = Umfang / 4;
        }
    }
}
