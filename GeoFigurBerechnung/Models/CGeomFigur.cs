﻿namespace GeoFigurBerechnung.Models
{
    // Basisklasse der geometrischen Figuren
    public class CGeomFigur
    {
        // Deklararion der vererbenden Eigenschaften
        internal string Bezeichnung { get; set; }
        internal string Farbe { get; set; }     
    }
}
