﻿using GeoFigurBerechnung.Interfaces;
using System;

namespace GeoFigurBerechnung.Models
{
    // Klasse erbt von CKoerper und von ICKoerper
    class MWuerfel : CKoerper, ICKoerper
    {
        // Deklaration der Eigenschaft
        internal double Seitenlaenge { get; set; }

        // Konstruktor mit Kontrollfluss und Aufruf von Funktionen zur Berechnung der Parameter
        public MWuerfel(string bezeichnung = "", string farbe = "", double seitenlaenge = 0, double oberflaeche = 0, double volumen = 0)
        {
            Bezeichnung = bezeichnung;
            Farbe = farbe;

            if (seitenlaenge > 0)
            {
                Seitenlaenge = seitenlaenge;
                Volumenberechnung();
                Oberflaechenberechnung();
            }
            else if (oberflaeche > 0)
            {
                Oberflaeche = oberflaeche;
                Seitenlaengenberechnung();
                Volumenberechnung();
            }
            else if (volumen > 0)
            {
                Volumen = volumen;
                Seitenlaengenberechnung();
                Oberflaechenberechnung();
            }
        }

        // Funktion zum brechnen der Seitenlänge
        public void Seitenlaengenberechnung()
        {
            if(Volumen > 0)
            {
                Seitenlaenge = Math.Ceiling(Math.Pow(Volumen, (double)1 / 3));
            }

            else if (Oberflaeche > 0)
            {
                Seitenlaenge = Math.Sqrt(Oberflaeche / 6);
            }
        }

        // Funktion zum berechnen des Volumens
        public void Volumenberechnung()
        {
            Volumen = Math.Pow(Seitenlaenge, 3);
        }

        // Funktion zum brechnen der Oberfläche
        public void Oberflaechenberechnung()
        {
            Oberflaeche = 6 * Math.Pow(Seitenlaenge, 2);
        }
    }
}
