﻿using System;
using GeoFigurBerechnung.Interfaces;

namespace GeoFigurBerechnung.Models
{
    // Klasse erbt von CFlaeche
    class MKreis : CFlaeche, ICFlaeche
    {
        // Deklaration der Eigenschaft Radius
        internal double Radius { get; set; }

        // Konstruktor mit Kontrollfluss der eingegebenen Daten & Aufruf verschiedener Funktionen
        public MKreis(string bezeichnung = "", string farbe = "", double radius = 0, double umfang = 0, double flaeche = 0)
        {
            Bezeichnung = bezeichnung;
            Farbe = farbe;

            if (radius > 0)
            {
                Radius = radius;
                Flaechenberechnung();
                Umfangberechnung();
            }
            else if (umfang > 0)
            {
                Umfang = umfang;
                Radius = RadiusBerechnung(umfang: umfang);
                Flaechenberechnung();
            }
            else if (flaeche > 0)
            {
                Flaecheninhalt = flaeche;
                Radius = RadiusBerechnung(flaeche: flaeche);
                Umfangberechnung();
            }
        }

        // Funktion zur Berechnung der Fläche
        public void Flaechenberechnung()
        {
            Flaecheninhalt = Math.PI * Math.Pow(Radius, 2);
        }

        // Funktion zur Berechnung des Umfangs
        public void Umfangberechnung()
        {
            Umfang = 2 * Math.PI * Radius;
        }        

        // Funktion zur Berechnung des Radiuses
        public double RadiusBerechnung(double umfang = 0, double flaeche = 0)
        {
            if (umfang > 0) return umfang / (2 * Math.PI);
            else return Math.Sqrt(flaeche / Math.PI);
        }
    }
}
