﻿namespace GeoFigurBerechnung.Interfaces
{
    // Interfaces die bei Vererbung, die erbende Klasse vorschreibt, welche Funktionen implementiert werden müssen
    public interface ICFlaeche
    {
        void Flaechenberechnung();
        void Umfangberechnung();
    }

    public interface ICKoerper
    {
        void Volumenberechnung();
        void Oberflaechenberechnung();
    }
}
