﻿using GeoFigurBerechnung.Views;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace GeoFigurBerechnung
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
 
    // Klasse erbt von Window
    public partial class MainWindow : Window
    {
        // Konstruktor der Willkommen Page am Anfang anzeigt
        public MainWindow()
        {
            InitializeComponent();
            this.MainFrame.Navigate(new Welcome());
        }

        // Event wenn Menüpunkt angeklickt wird
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            App.ParentWindowRef = this; // Festlegung des Hauptfensters
        
            switch (item.Name) // Kontrollfluss: je nach angeklickten Menüpunkt, wird in den dazugehörigen Fall gesprungen
            {
                case "MenuQuadrat": //Fallüberprüfung
                    {
                        SetBackgroundFromMenuItem(item); // Funktionsaufruf um Hintergrund zu ändern
                        this.MainFrame.Navigate(new Quadrat()); // Erzeugung der View und anzeige im Hauptfenster
                        break;
                    }
                case "MenuDreieck":
                    {
                        SetBackgroundFromMenuItem(item);
                        this.MainFrame.Navigate(new Dreieck());
                        break;
                    }
                case "MenuKreis":
                    {
                        SetBackgroundFromMenuItem(item);
                        this.MainFrame.Navigate(new Kreis());
                        break;
                    }
                case "MenuRaute":
                    {
                        SetBackgroundFromMenuItem(item);
                        this.MainFrame.Navigate(new Raute());
                        break;
                    }
                case "MenuWuerfel":
                    {
                        SetBackgroundFromMenuItem(item);
                        this.MainFrame.Navigate(new Wuerfel());
                        break;
                    }
                case "MenuPyramide":
                    {
                        SetBackgroundFromMenuItem(item);
                        this.MainFrame.Navigate(new Pyramide());
                        break;
                    }
                case "MenuKugel":
                    {
                        SetBackgroundFromMenuItem(item);
                        this.MainFrame.Navigate(new Kugel());
                        break;
                    }
            }
        }

        // Funktion die die Hintergrundfarbe für den angeklickten Menüpunkt und die nicht angeklickten Menüpunkte setzt
        private void SetBackgroundFromMenuItem(MenuItem item)
        {
            var bgClicked = new BrushConverter(); // Initialisierung des BrushConverters für den angeklickten Menüpunkt
            var bgNormal = new BrushConverter(); // Initialisierung des BrushConverters für die nicht angeklickten Menüpunkte
            item.Background = (Brush)bgClicked.ConvertFrom("#808080"); // Hintergrundfarbe des angeklickten Menüpunktes ändern

            foreach (Control menuItem in MainMenu.Items) // Für jedes Controll im Mainmenü
            {
                if (menuItem is MenuItem) // Überprüfung auf Menüitem
                {
                    if (menuItem.Name != item.Name) // Überprüfung auf nicht angeklicktes Menüitem
                    {
                        menuItem.Background = (Brush)bgNormal.ConvertFrom("#3F3F46"); // Hintergrundfarbe ändern
                    }
                }
            }
        }
    }
}
