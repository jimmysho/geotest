﻿using GeoFigurBerechnung.Models;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GeoFigurBerechnung.Views
{
    /// <summary>
    /// Interaction logic for Raute.xaml
    /// </summary>

    // Klasse erbt von Page
    public partial class Raute : Page
    {
        double _seitenlaenge;
        double _diagonaleE;
        double _diagonaleF;

        bool _seitenlangeIsBool;
        bool _diagonaleEIsBool;
        bool _diagonaleFIsBool;

        // Konstruktor
        public Raute()
        {
            InitializeComponent();
            lbWarnung.Visibility = Visibility.Hidden; // Warnung ausblenden
        }

        // Button "Berechnen" Click-Event
        private void btBerechnen_Click(object sender, RoutedEventArgs e)
        {         
            lbWarnung.Visibility = Visibility.Hidden;
            lbWarnung.Content = "Falsche Eingabe!!!"; // Warnung initialisieren

            if (CheckIfOnlyOneDoubleValue()) // Funktionsaufruf um die Eingabe auf double-Wert zu überprüfen 
            {
                MRaute raute = new MRaute(tbBezeichnung.Text, tbFarbe.Text, _seitenlaenge, _diagonaleE, _diagonaleF); // Erzeugung des MRaute Objektes

                if((!Double.IsNaN(raute.DiagonaleE) && !Double.IsInfinity(raute.DiagonaleE)) && (!Double.IsNaN(raute.DiagonaleF) && !Double.IsInfinity(raute.DiagonaleF))) // Überprüfung ob Werte eine mögliche Figur beschreiben
                {
                    // Füllen der Controlls
                    lbBezeichnung.Content = raute.Bezeichnung == "" ? "Bezeichnung: Keine Angabe" : "Bezeichnung: " + raute.Bezeichnung;
                    lbFarbe.Content = raute.Farbe == "" ? "Farbe: Keine Angabe" : "Farbe: " + raute.Farbe;
                    tbSeitenlaenge.Text = raute.Seitenlaenge.ToString();
                    tbDiagonaleE.Text = raute.DiagonaleE.ToString();
                    tbDiagonaleF.Text = raute.DiagonaleF.ToString();
                    tbflaecheninhalt.Text = raute.Flaecheninhalt.ToString();
                    tbUmfang.Text = raute.Umfang.ToString();
                }

                else
                {
                    lbWarnung.Content = "Eingabe beschreibt unmögliche Figur!!!";
                    lbWarnung.Visibility = Visibility.Visible; // Warnung einblenden
                }
            }
            else
            {
                lbWarnung.Visibility = Visibility.Visible; // Warnung einblenden
            }

        }

        // Funktion zur Überprüfung auf double-Wert
        private bool CheckIfOnlyOneDoubleValue()
        {
            _seitenlangeIsBool = Double.TryParse(tbSeitenlaenge.Text, out _seitenlaenge);
            _diagonaleEIsBool = Double.TryParse(tbDiagonaleE.Text, out _diagonaleE);
            _diagonaleFIsBool = Double.TryParse(tbDiagonaleF.Text, out _diagonaleF);

            if (new[] { _seitenlangeIsBool == true, _diagonaleEIsBool == true, _diagonaleFIsBool == true }.Count(x => x) == 2) return true;

            return false;
        }
    }
}
