﻿using GeoFigurBerechnung.Models;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GeoFigurBerechnung.Views
{
    /// <summary>
    /// Interaction logic for quadrat.xaml
    /// </summary>
    
    // Klasse erbt von Page
    public partial class Quadrat : Page
    {
        // Deklaration von Variablen
        double _flaecheninhalt;
        double _seitenlaenge;
        double _umfang;

        bool _flaecheninhaltIsBool;
        bool _seitenlangeIsBool;
        bool _umfangIsBool;

        // Konstruktor
        public Quadrat()
        {
            InitializeComponent();
            lbWarnung.Visibility = Visibility.Hidden; // Warnung ausblenden
        }

        // Button "Berechnen" Click-Event
        private void btBerechnen_Click(object sender, RoutedEventArgs e)
        {
            lbWarnung.Visibility = Visibility.Hidden;
            if (CheckIfOnlyOneDoubleValue()) // Funktionsaufruf um die Eingabe auf double-Wert zu überprüfen
            {
                MQuadrat quadrat = new MQuadrat(tbBezeichnung.Text, tbFarbe.Text, _seitenlaenge, _flaecheninhalt, _umfang); // Erzeugung des Mquadrat Objektes

                // Füllen der Controlls
                lbBezeichnung.Content = quadrat.Bezeichnung == "" ? "Bezeichnung: Keine Angabe" :  "Bezeichnung: " + quadrat.Bezeichnung;
                lbFarbe.Content = quadrat.Farbe == "" ? "Farbe: Keine Angabe" : "Farbe: " + quadrat.Farbe;
                tbSeitenlaenge.Text = quadrat.Seitenlaenge.ToString();
                tbFlaecheninhalt.Text = quadrat.Flaecheninhalt.ToString();
                tbUmfang.Text = quadrat.Umfang.ToString();
            }
            else
            {
                lbWarnung.Visibility = Visibility.Visible; // Warnung einblenden
            }             
        }

        // Funktion zur Überprüfung auf double-Wert
        private bool CheckIfOnlyOneDoubleValue()
        {
            _flaecheninhaltIsBool = Double.TryParse(tbFlaecheninhalt.Text, out _flaecheninhalt);
            _seitenlangeIsBool = Double.TryParse(tbSeitenlaenge.Text, out _seitenlaenge);
            _umfangIsBool = Double.TryParse(tbUmfang.Text, out _umfang);

            if (new[] { _flaecheninhaltIsBool == true, _seitenlangeIsBool == true, _umfangIsBool == true }.Count(x => x) == 1) return true;

            return false;
        }
    }
}
