﻿using GeoFigurBerechnung.Models;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GeoFigurBerechnung.Views
{
    /// <summary>
    /// Interaction logic for Dreieck.xaml
    /// </summary>

    // Klasse erbt von Page
    public partial class Dreieck : Page
    {
        // Deklaration von Variablen
        double _seiteA;
        double _seiteB;
        double _seiteC;

        bool _seiteAIsBool;
        bool _seiteBIsBool;
        bool _seiteCIsBool;

        // Konstruktor
        public Dreieck()
        {
            InitializeComponent();
            lbWarnung.Visibility = Visibility.Hidden;
        }

        // Button "Berechnen" Click-Event
        private void btBerechnen_Click(object sender, RoutedEventArgs e)
        {
            lbWarnung.Visibility = Visibility.Hidden; // Warnung wird versteckt
            lbWarnung.Content = "Falsche Eingabe!!!"; // Initialisierung des Warntextes
            if (CheckIfOnlyOneDoubleValue()) // Funktionsaufruf um die Eingabe auf double-Wert zu überprüfen
            {
                MDreieck dreieck = new MDreieck(tbBezeichnung.Text, tbFarbe.Text, _seiteA, _seiteB, _seiteC); // Erzeugung des MDreieck Objektes        

                if (!Double.IsNaN(dreieck.Flaecheninhalt) && !Double.IsInfinity(dreieck.Flaecheninhalt) && dreieck.Flaecheninhalt > 0) // Überprüfung ob Werte eine mögliche Figur beschreiben
                {
                    // Füllen der Controlls
                    lbBezeichnung.Content = dreieck.Bezeichnung == "" ? "Bezeichnung: Keine Angabe" : "Bezeichnung: " + dreieck.Bezeichnung;
                    lbFarbe.Content = dreieck.Farbe == "" ? "Farbe: Keine Angabe" : "Farbe: " + dreieck.Farbe;
                    tbSeiteA.Text = dreieck.SeiteA.ToString();
                    tbSeiteB.Text = dreieck.SeiteB.ToString();
                    tbSeiteC.Text = dreieck.SeiteC.ToString();
                    tbflaecheninhalt.Text = dreieck.Flaecheninhalt.ToString();
                    tbUmfang.Text = dreieck.Umfang.ToString();
                }

                else
                {
                    lbWarnung.Content = "Eingabe beschreibt unmögliche Figur!!!";
                    lbWarnung.Visibility = Visibility.Visible; // Anzeigen der Warnung
                }
            }
            else
            {
                lbWarnung.Visibility = Visibility.Visible; // Anzeigen der Warnung
            }

        }

        // Funktion zur Überprüfung auf double-Wert
        private bool CheckIfOnlyOneDoubleValue()
        {
            _seiteAIsBool = Double.TryParse(tbSeiteA.Text, out _seiteA);
            _seiteBIsBool = Double.TryParse(tbSeiteB.Text, out _seiteB);
            _seiteCIsBool = Double.TryParse(tbSeiteC.Text, out _seiteC);

            if (new[] { _seiteAIsBool == true, _seiteBIsBool == true, _seiteCIsBool == true }.Count(x => x) == 3) return true;

            return false;
        }
    }
}
