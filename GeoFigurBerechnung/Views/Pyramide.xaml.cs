﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using GeoFigurBerechnung.Models;

namespace GeoFigurBerechnung.Views
{
    /// <summary>
    /// Interaction logic for Pyramide.xaml
    /// </summary>

    //Klasse erbt von Page
    public partial class Pyramide : Page
    {
        double _seitenlaenge;
        double _hoehe;
        double _volumen;
        double _oberflaeche;

        bool _seitenlangeIsBool;
        bool _volumenIsBool;
        bool _oberflaecheIsBool;
        bool _hoeheIsBool;

        // Konstruktor
        public Pyramide()
        {
            InitializeComponent();
            lbWarnung.Visibility = Visibility.Hidden; // Warnung ausblenden
        }

        // Button "Berechnen" Click-Event
        private void btBerechnen_Click(object sender, RoutedEventArgs e)
        {
            lbWarnung.Visibility = Visibility.Hidden;
            lbWarnung.Content = "Falsche Eingabe!!!"; // Warnung initialisieren

            if (CheckIfOnlyOneDoubleValue()) // Funktionsaufruf um die Eingabe auf double-Wert zu überprüfen 
            {
                MPyramide pyramide = new MPyramide(tbBezeichnung.Text, tbFarbe.Text, _seitenlaenge, _hoehe, _volumen, _oberflaeche); // Erzeugung des MRaute Objektes

                if(_volumen > 0 && _oberflaeche > 0 || _oberflaeche > 0 && _hoehe > 0) // Überprüfung ob Berechnung der Parameter möglich
                {
                    lbWarnung.Visibility = Visibility.Visible;
                    lbWarnung.Content = "Nicht möglich!!!";
                    return;
                }

                if ((!Double.IsNaN(pyramide.Volumen) && !Double.IsInfinity(pyramide.Volumen)) && (!Double.IsNaN(pyramide.Oberflaeche) && !Double.IsInfinity(pyramide.Oberflaeche))) // Überprüfung ob Werte eine mögliche Figur beschreiben
                {
                    // Füllen der Controlls
                    lbBezeichnung.Content = pyramide.Bezeichnung == "" ? "Bezeichnung: Keine Angabe" : "Bezeichnung: " + pyramide.Bezeichnung;
                    lbFarbe.Content = pyramide.Farbe == "" ? "Farbe: Keine Angabe" : "Farbe: " + pyramide.Farbe;
                    tbSeitenlaenge.Text = pyramide.Seitenlaenge.ToString();
                    tbHoehe.Text = pyramide.Hoehe.ToString();
                    tbVolumen.Text = pyramide.Volumen.ToString();
                    tbOberflaeche.Text = pyramide.Oberflaeche.ToString();
                }

                else
                {
                    lbWarnung.Content = "Eingabe beschreibt unmögliche Figur!!!";
                    lbWarnung.Visibility = Visibility.Visible; // Warnung einblenden
                }
            }
            else
            {
                lbWarnung.Visibility = Visibility.Visible; // Warnung einblenden
            }
        }

        // Funktion zur Überprüfung auf double-Wert
        private bool CheckIfOnlyOneDoubleValue()
        {
            _seitenlangeIsBool = Double.TryParse(tbSeitenlaenge.Text, out _seitenlaenge);
            _hoeheIsBool = Double.TryParse(tbHoehe.Text, out _hoehe);
            _volumenIsBool = Double.TryParse(tbVolumen.Text, out _volumen);
            _oberflaecheIsBool = Double.TryParse(tbOberflaeche.Text, out _oberflaeche);

            if (new[] { _seitenlangeIsBool == true, _hoeheIsBool == true, _volumenIsBool == true, _oberflaecheIsBool == true }.Count(x => x) == 2) return true;

            return false;
        }
    }
}
