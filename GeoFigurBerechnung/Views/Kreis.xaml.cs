﻿using GeoFigurBerechnung.Models;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GeoFigurBerechnung.Views
{
    /// <summary>
    /// Interaction logic for Kreis.xaml
    /// </summary>

    // Klasse erbt von Page
    public partial class Kreis : Page
    {
        // Deklaration von Variablen
        double _Radius;
        double _Umfang;
        double _Flaecheninhalt;

        bool _radiusIsBool;
        bool _UmfangIsBool;
        bool _FlaecheninhaltIsBool;

        // Konstruktor
        public Kreis()
        {
            InitializeComponent();
            lbWarnung.Visibility = Visibility.Hidden;
        }

        // Button "Berechnen" Click-Event
        private void btBerechnen_Click(object sender, RoutedEventArgs e)
        {
            lbWarnung.Visibility = Visibility.Hidden; // Warnung ausblenden
            lbWarnung.Content = "Falsche Eingabe!!!"; // Warnung Initialisierung
            if (CheckIfOnlyOneDoubleValue())
            {
                MKreis kreis = new MKreis(tbBezeichnung.Text, tbFarbe.Text, _Radius, _Umfang, _Flaecheninhalt); // Initialisierung des MKreis Objektes

                // Füllen der Controlls
                lbBezeichnung.Content = kreis.Bezeichnung == "" ? "Bezeichnung: Keine Angabe" : "Bezeichnung: " + kreis.Bezeichnung;
                lbFarbe.Content = kreis.Farbe == "" ? "Farbe: Keine Angabe" : "Farbe: " + kreis.Farbe;
                tbRadius.Text = kreis.Radius.ToString();
                tbUmfang.Text = kreis.Umfang.ToString();
                tbFlaecheninhalt.Text = kreis.Flaecheninhalt.ToString();
            }
            else
            {
                lbWarnung.Visibility = Visibility.Visible; // Warnung einblenden
            }
        }

        // Funktion zur Überprüfung auf Double-Wert
        private bool CheckIfOnlyOneDoubleValue()
        {
            _radiusIsBool = Double.TryParse(tbRadius.Text, out _Radius);
            _UmfangIsBool = Double.TryParse(tbUmfang.Text, out _Umfang);
            _FlaecheninhaltIsBool = Double.TryParse(tbFlaecheninhalt.Text, out _Flaecheninhalt);

            if (new[] { _radiusIsBool == true, _UmfangIsBool == true, _FlaecheninhaltIsBool == true }.Count(x => x) == 1) return true;

            return false;
        }
    }
}
