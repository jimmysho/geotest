﻿using GeoFigurBerechnung.Models;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GeoFigurBerechnung.Views
{
    /// <summary>
    /// Interaction logic for Kugel.xaml
    /// </summary>
    
    // Klasse erbt von Page
    public partial class Kugel : Page
    {
        // Deklaration von Variablen
        double _oberflaeche;
        double _radius;
        double _volumen;

        bool _volumenIsBool;
        bool _radiusIsBool;
        bool _oberflaecheIsBool;

        // Konstruktor
        public Kugel()
        {
            InitializeComponent();
            lbWarnung.Visibility = Visibility.Hidden; // Warnung ausblenden
        }

        // Button "Berechnen" Click-Event
        private void btBerechnen_Click(object sender, RoutedEventArgs e)
        {
            lbWarnung.Visibility = Visibility.Hidden;
            if (CheckIfOnlyOneDoubleValue()) // Funktionsaufruf um die Eingabe auf double-Wert zu überprüfen
            {
                MKugel kugel = new MKugel(tbBezeichnung.Text, tbFarbe.Text, _radius, _oberflaeche, _volumen); // Erzeugung des MViereck Objektes

                // Füllen der Controlls
                lbBezeichnung.Content = kugel.Bezeichnung == "" ? "Bezeichnung: Keine Angabe" : "Bezeichnung: " + kugel.Bezeichnung;
                lbFarbe.Content = kugel.Farbe == "" ? "Farbe: Keine Angabe" : "Farbe: " + kugel.Farbe;
                tbRadius.Text = kugel.Radius.ToString();
                tbVolumen.Text = kugel.Volumen.ToString();
                tbOberflaeche.Text = kugel.Oberflaeche.ToString();
            }
            else
            {
                lbWarnung.Visibility = Visibility.Visible; // Warnung einblenden
            }
        }

        // Funktion zur Überprüfung auf double-Wert
        private bool CheckIfOnlyOneDoubleValue()
        {
            _volumenIsBool = Double.TryParse(tbVolumen.Text, out _volumen);
            _radiusIsBool = Double.TryParse(tbRadius.Text, out _radius);
            _oberflaecheIsBool = Double.TryParse(tbOberflaeche.Text, out _oberflaeche);

            if (new[] { _volumenIsBool == true, _radiusIsBool == true, _oberflaecheIsBool == true }.Count(x => x) == 1) return true;

            return false;
        }
    }
}
