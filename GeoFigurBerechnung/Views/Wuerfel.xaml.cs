﻿using GeoFigurBerechnung.Models;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace GeoFigurBerechnung.Views
{
    /// <summary>
    /// Interaction logic for Wuerfel.xaml
    /// </summary>
    
    // Klasse erbt von Page
    public partial class Wuerfel : Page
    {
        // Deklaration von Variablen
        double _oberflaeche;
        double _seitenlaenge;
        double _volumen;

        bool _volumenIsBool;
        bool _seitenlangeIsBool;
        bool _oberflaecheIsBool;

        // Konstruktor
        public Wuerfel()
        {
            InitializeComponent();
            lbWarnung.Visibility = Visibility.Hidden; // Warnung ausblenden
        }

        // Button "Berechnen" Click-Event
        private void btBerechnen_Click(object sender, RoutedEventArgs e)
        {
            lbWarnung.Visibility = Visibility.Hidden;
            if (CheckIfOnlyOneDoubleValue()) // Funktionsaufruf um die Eingabe auf double-Wert zu überprüfen
            {
                MWuerfel wuerfel = new MWuerfel(tbBezeichnung.Text, tbFarbe.Text, _seitenlaenge, _oberflaeche, _volumen); // Erzeugung des MViereck Objektes

                // Füllen der Controlls
                lbBezeichnung.Content = wuerfel.Bezeichnung == "" ? "Bezeichnung: Keine Angabe" : "Bezeichnung: " + wuerfel.Bezeichnung;
                lbFarbe.Content = wuerfel.Farbe == "" ? "Farbe: Keine Angabe" : "Farbe: " + wuerfel.Farbe;
                tbSeitenlaenge.Text = wuerfel.Seitenlaenge.ToString();
                tbVolumen.Text = wuerfel.Volumen.ToString();
                tbOberflaeche.Text = wuerfel.Oberflaeche.ToString();
            }
            else
            {
                lbWarnung.Visibility = Visibility.Visible; // Warnung einblenden
            }
        }

        // Funktion zur Überprüfung auf double-Wert
        private bool CheckIfOnlyOneDoubleValue()
        {
            _volumenIsBool = Double.TryParse(tbVolumen.Text, out _volumen);
            _seitenlangeIsBool = Double.TryParse(tbSeitenlaenge.Text, out _seitenlaenge);
            _oberflaecheIsBool = Double.TryParse(tbOberflaeche.Text, out _oberflaeche);

            if (new[] { _volumenIsBool == true, _seitenlangeIsBool == true, _oberflaecheIsBool == true }.Count(x => x) == 1) return true;

            return false;
        }
    }
}
