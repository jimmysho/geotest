﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeoFigurBerechnung.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeoFigurBerechnung.Models.Tests
{
    [TestClass()]
    public class RauteTests
    {
        [TestMethod()]
        public void WinkelAlphaBerechnungTest()
        {
            double diagonale = 8;
            double seite = 10;
            double expected = 106.26;
            MRaute raute = new MRaute("raute", "rot");
            double actual = raute.WinkelAlphaBerechnung(seite, diagonale);
            Assert.AreEqual(expected, actual);
        }
    }
}